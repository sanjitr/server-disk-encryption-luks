# Server Encryption LUKS

This project helps with encrypting, via [LUKS](https://gitlab.com/cryptsetup/cryptsetup/), all disks on a server. It is based on [https://github.com/TheReal1604/disk-encryption-hetzner/blob/master/ubuntu/ubuntu_swraid_lvm_luks.md](https://github.com/TheReal1604/disk-encryption-hetzner/blob/master/ubuntu/ubuntu_swraid_lvm_luks.md) and [https://seeseekey.net/archive/122144/](https://seeseekey.net/archive/122144/).
Both guides are for encrypting disks on a [Hetzner](https://www.hetzner.de/) Server, which was also my primary use-case.
The steps of both guides are packed into shell scripts for a faster setup of a new server.
As long as you can access the drives from an external system, e.g., a rescue system, you should be able to use those scripts.
In the future, I will add scripts for encrypting the SD card of a Raspberry Pi.

## Prerequisites

The scripts take following for granted:

- Ubuntu 18.04 as the OS of the Server
- You run them as root (except for the client ones)
- SSH Access to the system
- Usage of [LVM](https://wiki.ubuntu.com/Lvm)
- Three logical volumes root (/), home (/home), and opt (/opt)
- A RAID which can be found at /dev/md1

You can contribute and reduce the number of prerequisites the scripts need.

## Result

The scripts will backup all data, encrypt the RAID system, re-create the LVM, and restore the backup.
The system will boot up in [BusyBox](https://busybox.net/) and be accessible via [Dropbear SSH](https://matt.ucc.asn.au/dropbear/dropbear.html). When you connect, you can enter the LUKS key, and at that moment in time, the installed system will boot up.

## Usage

The directory structure contains a client and a server folder. Scripts are numbered and need to be called in that order.
Each Linux distribution and system might need its own set of scripts.
Instructions for:

- [Ubuntu 18.04](server/ubuntu18.04/README.md)
