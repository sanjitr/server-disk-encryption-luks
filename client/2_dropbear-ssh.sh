#!/bin/bash
SSH_USERNAME=$1
SSH_HOST=$2

ssh-keygen -t rsa -b 4096 -f ~/.ssh/dropbear
scp ~/.ssh/dropbear.pub $SSH_USERNAME@$SSH_HOST:/etc/initramfs-tools/root/.ssh/authorized_keys
