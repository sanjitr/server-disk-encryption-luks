# Server Encryption LUKS for Ubuntu 18.04

For general prerequisites, check the main [README.md](../../README.md) in this repository.

## Usage

1. Copy `1_preperation.sh` and `5_optional.sh` to your server.
2. Execute `1_preperation.sh`
3. Execute `2_dropbear-ssh.sh <SSH_USERNAME> <SSH_HOST>` on your client.
4. Start your server into rescue mode or any similar mode.
5. Copy `3_setup_luks.sh` and `4_setup_grub.sh` to your server
6. Execute `3_setup_luks.sh` and `4_setup_grub.sh`
7. Reboot your server
8. Now unlock the system with `ssh -i .ssh/dropbear root@<yourserverip>`
9. Call `cryptroot-unlock` and enter your LUKS passphrase
10. Exit dropbear ssh

Your machine should continue to boot, and you should be able to login with your regular user via ssh.

### Optional

Now you can execute the optional script `5_optional.sh`. This script will configure dropbear to listen to port 2222 instead of 22, disable password logins, disable port forwarding, and set an idle timeout to 30.
Additionally, it will alter the `/etc/initramfs-tools/root/.ssh/authorized_keys` file with `no-port-forwarding,no-agent-forwarding,no-X11-forwarding,command="/bin/cryptroot-unlock"`.
Reboot your server, and now you can unlock your system by

```shell
ssh -p 2222 -i .ssh/dropbear root@<yourserverip>
```

You just have to enter the LUKS key, exit the bash, and will now be able to SSH into your system.
