#!/bin/bash
sed -i 's/.*DROPBEAR_OPTIONS=.*/DROPBEAR_OPTIONS="-p 2222 -s -j -k -I 30"/' /etc/dropbear-initramfs/config
sed -i '1s|^|no-port-forwarding,no-agent-forwarding,no-X11-forwarding,command="/bin/cryptroot-unlock" |' /etc/initramfs-tools/root/.ssh/authorized_keys
cp /etc/initramfs-tools/root/.ssh/authorized_keys /etc/dropbear-initramfs/authorized_keys
update-initramfs -u
