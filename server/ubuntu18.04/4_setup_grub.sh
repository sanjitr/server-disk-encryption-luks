#!/bin/bash
update-grub
grub-install /dev/sda
grub-install /dev/sdb
exit
umount /mnt/boot /mnt/home /mnt/opt /mnt/proc /mnt/sys /mnt/dev
umount /mnt
sync
