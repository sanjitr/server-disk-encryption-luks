#!/bin/bash
mkdir /oldroot/
mount /dev/mapper/vg0-root /mnt/
mount /dev/mapper/vg0-home /mnt/home
mount /dev/mapper/vg0-opt /mnt/opt
rsync -a /mnt/ /oldroot/
umount /mnt/home/
umount /mnt/opt/
umount /mnt/

vgremove vg0
cryptsetup --cipher aes-xts-plain64 --key-size 256 --hash sha256 --iter-time 6000 luksFormat /dev/md1
cryptsetup luksOpen /dev/md1 cryptroot
pvcreate /dev/mapper/cryptroot
vgcreate vg0 /dev/mapper/cryptroot
lvcreate -n swap -L16G vg0
lvcreate -n root -L100G vg0
lvcreate -n home -L50G vg0
lvcreate -n opt -L50G vg0

mkfs.ext4 /dev/vg0/root
mkfs.ext4 /dev/vg0/opt
mkfs.ext4 /dev/vg0/home
mkswap /dev/vg0/swap
mount /dev/vg0/root /mnt/
mkdir /mnt/home /mnt/opt
mount /dev/vg0/opt /mnt/opt/
mount /dev/vg0/home /mnt/home
rsync -a /oldroot/ /mnt/
mount /dev/md0 /mnt/boot
mount --bind /dev /mnt/dev
mount --bind /sys /mnt/sys
mount --bind /proc /mnt/proc
chroot /mnt
echo "\ncryptroot /dev/md1 none luks" >> /etc/crypttab
cp /etc/initramfs-tools/root/.ssh/authorized_keys /etc/dropbear-initramfs/
update-initramfs -u
